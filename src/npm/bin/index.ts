#!/usr/bin/env node

/**
 * Main entry point for trikata npm bin script.
 */
require('@babel/register')({ extensions: ['.js', '.jsx', '.ts', '.tsx'] });
import '@babel/polyfill';
import program from 'commander';
import bunyan from 'bunyan';
import { format as formatSql } from "sql-formatter";
import { fork } from 'fluture';
import { promisify } from 'bluebird';
import { readFile as readFileCb, writeFile as writeFileCb } from 'fs';
import { start } from '../../server';
import { migrate, syncToFilesystem } from '../../migrations';
import { MigrationPlan, createFromSDLAndDB } from '../../migrations/plan';
import { migrationSql } from '../../migrations/sql';
import { PGDBContext, connect } from '../../db/pg';
import { log } from '../../util/logging';

const TYPEDEFS_PATH = 'index.graphql';
const INITIAL_TYPEDEFS = `
type User {
  id: ID! @unique
  email: String! @unique
  name: String!
}
`;

const readFile: (path: string, encoding: string) => any = promisify(readFileCb);
const writeFile: (path: string, data: string) => any = promisify(writeFileCb);
const loadTypedefs = (path = TYPEDEFS_PATH) => readFile(path, 'utf8');
const disableLogging = () => log.level(bunyan.FATAL + 1);


/**
 * init command
 */
program
    .command('init')
    .action(async () => {
        await writeFile(TYPEDEFS_PATH, INITIAL_TYPEDEFS);
        const pkg = await readFile('./package.json', 'utf8');
        const config = JSON.parse(pkg);
        config.scripts = config.scripts || {};
        config.scripts.migrate = 'node_modules/.bin/tk migrate';
        config.scripts.start = 'node_modules/.bin/tk start';
        await writeFile('./package.json', JSON.stringify(config, null, 2));
        await writeFile('.babelrc', JSON.stringify({
            extends: './node_modules/trikata/.babelrc'
        }));
    });

/**
 * migrate command
 */
program
    .command('migrate')
    .action(() => {
        disableLogging();
        console.log('Running migrations');
        const error = (e: any) => {
            console.error('Error running migrations:', e);
            process.exit(1);
        };
        const success = (result: any) => {
            console.log('Migrations ran successfully');
        };
        fork(error)(success)(migrate());
    });

/**
 * sync migrations to file system
 */
program
    .command('migrations:sync')
    .action(() => {
        disableLogging();
        const error = (e: any) => {
            console.error('Error syncing migrations:', e);
            process.exit(1);
        };
        const success = () => console.log('Migrations synced successfully');
        fork(error)(success)(syncToFilesystem());
    });

/**
 * start command
 */
program
    .command('start')
    .action(async () => {
        const typedefs = await loadTypedefs();
        await start(typedefs);
    });

program.parse(process.argv);

