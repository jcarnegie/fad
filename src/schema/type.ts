// @flow
import {
  DefinitionNode,
  DocumentNode,
  EnumTypeDefinitionNode,
  ObjectTypeDefinitionNode,
} from 'graphql/language';

import { tableize } from 'inflection';
import {
  append,
  clone,
  concat,
  contains,
  curry,
  filter,
  find,
  isNil,
  map,
  path,
  propEq,
  reduce,
} from 'ramda';

import { ParseSchemaContext } from './context';
import { Schema } from '.';
import { Field, hasDirective as fieldHasDirective, scalarFields, createTimestampField } from './field';
import { Index } from './idx';

import { createFieldFromFieldDef } from './field';
import { createIndexFromDirective, createIndexFromField } from './idx';
import { directives as parseDirectives } from './directives';

export type Type = {
  name: string,
  tableName: string,
  fields: Field[],
  typedJsonFields: Field[],
  indexes: Index[],
  directives: any,
};

export const isObjectTypeDef = (node: DefinitionNode): boolean =>
  node.kind === 'ObjectTypeDefinition';

export const isEnumTypeDef = (node: DefinitionNode): boolean =>
  node.kind === 'EnumTypeDefinition';

// Would use filter, but we need to cast node to ObjectTypeDefinitionNode
export const filterTypeDefs = (ast: DocumentNode): ObjectTypeDefinitionNode[] => 
  reduce((typedefs: any, node: DefinitionNode) =>
    isObjectTypeDef(node)
      ? append(<ObjectTypeDefinitionNode>node, typedefs)
      : typedefs, [], ast.definitions);  

export const filterEnumTypeDefs = (ast: DocumentNode): EnumTypeDefinitionNode[] =>
  reduce((typedefs: any, node: DefinitionNode) =>
    isEnumTypeDef(node)
      ? append(<EnumTypeDefinitionNode>node, typedefs)
      : typedefs, [], ast.definitions);

export const typeNameFromTypeDef = (node: ObjectTypeDefinitionNode): string =>
  path(['name', 'value'], node) || '';

export const filterIndexableFields = (type: Type) =>
  filter(f => (
    f.type === 'ID' ||
    !isNil(find(propEq('name', 'unique'), f.directives)) ||
    !isNil(find(propEq('name', 'index'), f.directives))
  ), type.fields);

// export const parseIndexes = (typedef: ObjectTypeDefinitionNode): any[] => {
//   const typeDirectives = parseDirectives(typedef);
//   return [];
// };

export const createTypeFromTypeDef =
  (context: ParseSchemaContext, typedef: ObjectTypeDefinitionNode): Type => {
    const directives = parseDirectives(typedef);

    const newType: Type = {
      name: typeNameFromTypeDef(typedef),
      tableName: tableize(typeNameFromTypeDef(typedef)),
      fields: [],
      typedJsonFields: [],
      indexes: [],
      directives,
    };

    // only include timestamp fields for non-@stateless types
    const timestampFields = hasDirective('stateless', newType)
      ? []
      : [
        createTimestampField(context, newType, 'createdAt'),
        createTimestampField(context, newType, 'updatedAt'),
      ];

    newType.fields = [
      ...map(createFieldFromFieldDef(context, newType), clone(typedef.fields)),
      ...timestampFields,
    ];
    const typeIndexes = map(createIndexFromDirective(newType), filter(propEq('name', 'index'), directives));
    const indexableFields = filterIndexableFields(newType);
    const fieldIndexes = map(createIndexFromField, indexableFields);
    newType.indexes = concat(typeIndexes, fieldIndexes);

    return newType;
  };

export const findFieldByName = (type: Type, fieldName: string): Field => {
  const field = find(propEq('name', fieldName), type.fields);
  if (!field) throw new Error(`Field ${fieldName} not found in type ${type.name}`);
  return field;
};

export const isTypedJsonField = (schema: Schema, field: Field): boolean => {
  // const typeNames = map(t => t.name, schema.statelessTypes);
  const isContained = contains(field.type, map(t => t.name, schema.statelessTypes));
  const isTyped = field.sqlType === 'jsonb' && isContained;
  return isTyped;
};

export const readableFields = (type: Type): Field[] =>
  filter(field => !fieldHasDirective('writeOnly', field), type.fields);

export const readableScalarFields = (type: Type): Field[] =>
  filter(field => !fieldHasDirective('writeOnly', field), scalarFields(type));

export const hasDirective = curry((name: string, type: Type): boolean =>
  find(propEq('name', name), type.directives) !== undefined);