// @flow
import { join, map } from 'ramda';
import { Enum } from '../enum';

export const renderEnum = (_enum: Enum): string => `
enum ${_enum.name} {
${join('\n', map(v => `  ${v}`, _enum.values))}
}
`;
