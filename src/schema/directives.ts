// @flow
import {
  ArgumentNode,
  FieldDefinitionNode,
  ObjectTypeDefinitionNode,
  ValueNode,
} from 'graphql/language';

import { map, merge, reduce } from 'ramda';

type Directive = {
  name: string,
  args: { [K: string]: any },
}

export const coerceArgValue = (value: ValueNode): any => {
  switch (value.kind) {
    case 'IntValue': return parseInt(value.value, 10);
    case 'FloatValue': return parseFloat(value.value);
    case 'StringValue': return value.value;
    case 'BooleanValue': return value.value;
    case 'NullValue': return null;
    case 'ListValue': return map(coerceArgValue, value.values);
    case 'ObjectValue': return reduce((acc, field) =>
      merge(acc, { [field.name.value]: coerceArgValue(field.value) }), {}, value.fields);
    default:
      throw new Error(`Unknown directive argument type '${value.kind}'`);
  }
};

export const directiveArg = (arg: ArgumentNode) => ({
  [arg.name.value]: coerceArgValue(arg.value),
});

export const directives = (field: FieldDefinitionNode | ObjectTypeDefinitionNode): Directive[] =>
  map(directive => {
    const name = directive.name.value;
    const args = reduce((acc, arg) => merge(acc, directiveArg(arg)), {}, directive.arguments || []);
    return { name, args };
  }, field.directives || []);
