import { FutureInstance, map, parallel } from 'fluture';
import { DB, SqlType } from '../db';
import { sqlType } from '../db/pg/meta';
import { Schema } from '../schema';
import { Scalar, loadScalarTypes } from '../schema/scalars';
import { CustomResolvers, loadCustomResolvers, loadDirectiveResolvers } from '../schema/resolvers';

export const SCALARS_DIR = process.env.NODE_ENV === 'test'
  ? 'src/scalars'
  : 'node_modules/trikata/lib/scalars';

export const RESOLVERS_DIR = process.env.NODE_ENV === 'test'
  ? 'src/resolvers'
  : 'node_modules/trikata/lib/resolvers';


export type ParseSchemaContext = {
  sqlType: (gqlType: string) => SqlType;
  scalars: Scalar[];
  resolvers: CustomResolvers,
  directiveResolvers: any,
};

export type RuntimeContext = {
  db: DB,
  schema: Schema,
  idFn: () => string,
};

export const createParseSchemaContext = (): FutureInstance<{}, ParseSchemaContext> => {
  const createContext = ([scalars, resolvers, directiveResolvers]: any) => ({
    sqlType,
    scalars,
    resolvers,
    directiveResolvers
  });
  const loaders = <FutureInstance<{}, [any, any, any]>>parallel(3)([
    loadScalarTypes([SCALARS_DIR, "scalars"]),
    loadCustomResolvers([RESOLVERS_DIR, "."]),
    loadDirectiveResolvers([]),
  ]);
  return map(createContext)(loaders);
};