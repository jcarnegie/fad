import { curry, filter, fromPairs, map, merge, pick, toPairs } from 'ramda';
import { hash } from 'argon2';
import { Type } from './type';
import { hasDirective, relationFields } from './field';
import { reduce } from 'bluebird';

export enum Action {
    BEFORE_RESOLVE = 'before_resolve',
    AFTER_RESOLVE = 'after_resolve',
};

export type Hook = {
    action: Action,
    callback: any,
    config: any,
};

export const loadHooks = (dir: string) => {

};

export const cryptHook = curry(async (type: Type, object: any, args: any, context: any, info: any) => {
    const cryptedFields = filter(hasDirective('crypted'), type.fields);
    const cryptedFieldNames = map(f => f.name, cryptedFields);
    const fieldsToCrypt = toPairs(pick(cryptedFieldNames, args.data));
    const updatedFields = await Promise.all(map(async pair => [
        pair[0],
        await hash(pair[1]),
    ], fieldsToCrypt));
    args.data = merge(args.data, fromPairs(updatedFields));

    // Todo: recurse through the args tree
    // const rFields = relationFields(type);
    // return await reduce(async (p, field) => {
    //     await p;
    //     return 
    // }, Promise.resolve(), rFields)
});
