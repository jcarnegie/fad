import moment from 'moment';
import { basename } from 'path';
import { createHash } from 'crypto';
import { go, parallel } from 'fluture';
import { differenceWith, map as rMap } from 'ramda';
import S from '../util/sanctuary';
import { MIGRATIONS_DIR, FutureMigration, FutureMigrationArray, Migration } from '.';
import { mkdir, readFile, readFileSafe, rmdir, safeDirectory, writeFile } from '../util/fs';

export const TS_FORMAT = 'YYYYMMDDHHmmss';

const cmpByCreatedAt = (a: any, b: any) => {
  return a.createdAt.getTime() === b.createdAt.getTime();
}

export const createMigrationsDir = (dir: string = MIGRATIONS_DIR) => mkdir(dir);

const loadMigration = (dir: string): FutureMigration => <FutureMigration>go(function*() {
    const createdAt = moment(basename(dir), TS_FORMAT).toDate();
    const typedefs = yield readFile(`${dir}/index.graphql`);
    const sql = yield readFile(`${dir}/migration.sql`);
    const beforeSql = yield readFileSafe(`${dir}/before.sql`);
    const afterSql = yield readFileSafe(`${dir}/after.sql`);
    const typedefsHash = createHash('sha1').update(typedefs).digest('hex');
    const sqlHash = createHash('sha1').update(sql).digest('hex');
    return {
      createdAt,
      typedefs,
      typedefsHash,
      sql,
      sqlHash,
      beforeSql,
      afterSql,
      beforeFiles: [],
      afterFiles: []
    };
  });

export const loadMigrations = (dir: string = MIGRATIONS_DIR): FutureMigrationArray => <FutureMigrationArray>go(function*() {
    const migrationDir = yield safeDirectory(dir);
    const loadMigrationFutures = rMap(loadMigration, migrationDir.filePaths);
    const migrations = yield parallel(Infinity)(loadMigrationFutures);
    return S.sortBy(S.prop("createdAt"))(migrations);
});

export const writeMigration = S.curry2((dir: string = MIGRATIONS_DIR, migration: Migration) => go(function* () {
  const timestamp = moment(migration.createdAt).format(TS_FORMAT);
  const migrationDir = `${dir}/${timestamp}`;
  const typedefsPath = `${migrationDir}/index.graphql`;
  const sqlPath = `${migrationDir}/migration.sql`;
  yield mkdir(migrationDir);
  yield writeFile(typedefsPath, migration.typedefs);
  yield writeFile(sqlPath, migration.sql);
}));

export const writeMigrations = (
  dir: string = MIGRATIONS_DIR,
  migrations: Migration[]
) => {
    const futures = S.map(writeMigration(dir))(migrations);
    return parallel(Infinity)(futures);
};

export const removeMigrations = (
  dir: string = MIGRATIONS_DIR,
  existingMigrations: Migration[],
  migrationsToApply: Migration[]
) => {
  const toRemove = differenceWith(cmpByCreatedAt, existingMigrations, migrationsToApply);
  const timestamp = (migration: Migration) => moment(migration.createdAt).format(TS_FORMAT);
  const remove = (migration: Migration) => rmdir(`${dir}/${timestamp(migration)}`);
  const futures = S.map(remove)(toRemove);
  return parallel(Infinity)(futures);
};