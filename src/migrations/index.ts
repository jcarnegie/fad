import uuid from 'uuid/v4';
import { createHash } from 'crypto';
import { FutureInstance, go, attemptP } from 'fluture';
import { append, differenceWith, findIndex, isEmpty, propEq, takeWhile } from 'ramda'
import { PGDBContext, connect } from '../db/pg';
import { MigrationPlan, create } from './plan';
import { migrationSql } from './sql';
import { createMigrationsDir, loadMigrations as loadFSMigrations, removeMigrations, writeMigrations } from './fs';
import { createMigrationsTable, loadMigrations as loadDBMigrations, applyMigrations } from './db';
import { sortAscByDateProp } from '../util/date';
import { createParseSchemaContext } from '../schema/context';
import { readFile } from '../util/fs';
import { parseTypedefs } from '../schema';
import { execStmt } from '../db/pg';
import { parseDBSchema } from '../db/pg/ddl';
import { createSchemaSql } from '../db/pg/sql';

export const MIGRATIONS_DIR = './migrations';

const cmpByCreatedAt = (a: any, b: any) => {
  return a.createdAt.getTime() === b.createdAt.getTime();
}

const hash = (contents: string) =>
  createHash("sha1")
    .update(contents)
    .digest("hex");

export type MigrationHookFile = {
  name: string,
  hash: string,
};

export type Migration = {
  createdAt: Date,
  typedefs: string,
  typedefsHash: string,
  sql: string,
  sqlHash: string,
  beforeSql?: string,
  afterSql?: string,
  beforeFiles: MigrationHookFile[],
  afterFiles: MigrationHookFile[],
};

export type FutureMigration = FutureInstance<{}, Migration>;
export type FutureMigrationArray = FutureInstance<{}, Migration[]>;

export const generate = (
  dbCtx: PGDBContext,
  currentTypedefs: string,
  plan: MigrationPlan,
  fileMigrations: Migration[],
  dbMigrations: Migration[]
): Migration[] => {
  const sortedFileMigrations = sortAscByDateProp('createdAt', fileMigrations);
  const sortedDbMigrations = sortAscByDateProp('createdAt', dbMigrations);
  const unappliedMigrations = differenceWith(cmpByCreatedAt, sortedFileMigrations, sortedDbMigrations);
  const firstUnappliedTimestamp = isEmpty(unappliedMigrations) ? null : unappliedMigrations[0].createdAt;
  const appliedMigrations = takeWhile((x: Migration) => x.createdAt !== firstUnappliedTimestamp, sortedFileMigrations);

  if (plan.actions.length === 0) return sortedFileMigrations;

  const sql = migrationSql(dbCtx, plan);
  return append({
    createdAt: firstUnappliedTimestamp || new Date(),
    typedefs: currentTypedefs,
    typedefsHash: hash(currentTypedefs),
    sql,
    sqlHash: hash(sql),
    beforeFiles: [],
    afterFiles: [],
  }, appliedMigrations);
};

export const syncToFilesystem = (dir: string = MIGRATIONS_DIR): FutureInstance<any, any> => go(function* () {
  const typedefs = yield readFile('./index.graphql');
  const parseSchemaContext = yield createParseSchemaContext();
  const schema = parseTypedefs(parseSchemaContext, typedefs);
  const client = yield attemptP(() => connect());
  const dbCtx: PGDBContext = {
    client,
    schema,
    schemaName: 'trikata',
    idFn: uuid,
  };
  yield attemptP(() => execStmt(dbCtx.client, createSchemaSql(dbCtx)));
  yield createMigrationsDir(dir);
  yield createMigrationsTable(dbCtx);
  const dbSchema = yield attemptP(() => parseDBSchema(dbCtx));
  const fsMigrations = yield loadFSMigrations(MIGRATIONS_DIR);
  const dbMigrations = yield loadDBMigrations(dbCtx);
  const plan = create(schema, dbSchema);
  const syncedMigrations = generate(dbCtx, typedefs, plan, fsMigrations, dbMigrations);
  yield removeMigrations(dir, fsMigrations, syncedMigrations);
  yield writeMigrations(dir, syncedMigrations);
  yield attemptP(() => client.end());
});

export const migrate = (dir: string = MIGRATIONS_DIR) => go(function* () {
  const typedefs = yield readFile('./index.graphql');
  const parseSchemaContext = yield createParseSchemaContext();
  const schema = parseTypedefs(parseSchemaContext, typedefs);
  const client = yield attemptP(() => connect());
  const dbCtx: PGDBContext = {
    client,
    schema,
    schemaName: 'trikata',
    idFn: uuid,
  }
  yield attemptP(() => execStmt(dbCtx.client, createSchemaSql(dbCtx)));
  yield createMigrationsDir(dir);
  yield createMigrationsTable(dbCtx);
  // const dbSchema = yield attemptP(() => parseDBSchema(dbCtx));
  const fsMigrations = yield loadFSMigrations(MIGRATIONS_DIR);
  const dbMigrations = yield loadDBMigrations(dbCtx);
  const sortedFileMigrations = sortAscByDateProp('createdAt', fsMigrations);
  const sortedDbMigrations = sortAscByDateProp('createdAt', dbMigrations);
  const unappliedMigrations = differenceWith(cmpByCreatedAt, sortedFileMigrations, sortedDbMigrations);
  const result = yield applyMigrations(dbCtx, unappliedMigrations);
  yield attemptP(() => client.end());
  return result;
});