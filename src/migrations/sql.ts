import S from '../util/sanctuary';
import { format as f } from 'sql-formatter';
import { append, contains, join } from "ramda";
import { PGDBContext } from '../db/pg';
import {
  addColumnSql,
  addIndexSql,
  alterColumnDefaultSql,
  alterColumnNullabilitySql,
  alterColumnTypeSql,
  createJoinTableSql,
  createTableSql,
  dropColumnSql,
  dropConstraintSql,
  dropIndexSql,
  dropTableSql,
  renameRelationForeignKeySql,
  renameTableSql,
} from "../db/pg/sql";
import { Index } from '../schema/idx';
import { createRelationType } from '../schema/relation';
import {
  CreateAddColumnAction,
  CreateAddIndexAction,
  CreateRelationTableAction,
  CreateRemoveIndexAction,
  CreateRemoveTableAction,
  CreateTableAction,
  CreateUpdateColumnAction,
  CreateRemoveColumnAction,
  MigrationAction,
  MigrationActions,
  MigrationPlan,
  RenameRelationTableAction,
} from "./plan";

// SQL statement separator
const SEP = ';\n\n';

export const migrationSql = (dbCtx: PGDBContext, plan: MigrationPlan): string =>
  join(SEP, S.map((action: MigrationAction) => {
    switch (action.actionType) {

      case MigrationActions.CREATE_TABLE:
        return f(createTableSql(dbCtx, (<CreateTableAction>action).type).sql);

      case MigrationActions.CREATE_RELATION_TABLE:
        const { relation } = (<CreateRelationTableAction>action);
        const joinTableSql = f(createJoinTableSql(dbCtx, relation).sql);
        const relationType = createRelationType(relation);
        const indexesSql = S.map((i: Index) => f(addIndexSql(dbCtx, i).sql))(relationType.indexes);
        return join(SEP, [joinTableSql, join(SEP, indexesSql)]);

      case MigrationActions.ADD_COLUMN:
        return f(addColumnSql(dbCtx, (<CreateAddColumnAction>action).field).sql);

      case MigrationActions.RENAME_RELATION_TABLE:
        const a = <RenameRelationTableAction><unknown>action;
        const renTableSql = f(renameTableSql(dbCtx, a.old.tableName, a.new.tableName).sql)
        const renFKSql = f(renameRelationForeignKeySql(dbCtx, a.old, a.new, a.new.srcType).sql);
        return join(SEP, [renTableSql, renFKSql]);

      case MigrationActions.ADD_INDEX:
        const addIndexAction = <CreateAddIndexAction>action;
        return f(addIndexSql(dbCtx, addIndexAction.index).sql);

      case MigrationActions.UPDATE_COLUMN:
        const updateColAction = <CreateUpdateColumnAction>action;
        const { field } = updateColAction;
        let updates: string[] = [];

        if (contains('type', updateColAction.changes)) {
          const typeSql = f(alterColumnTypeSql(dbCtx, field).sql);
          updates = append(typeSql, updates);
        }

        if (contains('default', updateColAction.changes)) {
          const defaultSql = f(alterColumnDefaultSql(dbCtx, field).sql);
          updates = append(defaultSql, updates);
        }

        if (contains('nullability', updateColAction.changes)) {
          const nullabilitySql = f(alterColumnNullabilitySql(dbCtx, field).sql);
          updates = append(nullabilitySql, updates);
        }
        return join(SEP, updates);

      case MigrationActions.REMOVE_COLUMN:
        const removeColAction = <CreateRemoveColumnAction>action;
        return f(dropColumnSql(dbCtx, removeColAction.field).sql);

      case MigrationActions.REMOVE_TABLE:
        const removeTblAction = <CreateRemoveTableAction>action;
        return f(dropTableSql(dbCtx, removeTblAction.type).sql);

      case MigrationActions.REMOVE_INDEX:
        const removeIdxAction = <CreateRemoveIndexAction>action;
        const { index } = removeIdxAction;
        return (index.primary || index.unique)
          ? f(dropConstraintSql(dbCtx, index.parentType, index.name).sql)
          : f(dropIndexSql(dbCtx, index).sql);

      default:
        return '';
    }
  })(plan.actions)) + ';';