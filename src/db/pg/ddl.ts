// @flow
import P from 'bluebird';
import { QueryResult } from 'pg';
import {
  append,
  ascend,
  assoc,
  contains,
  curry,
  find,
  head,
  length,
  map,
  prop,
  propEq,
  reduce,
  sort,
  split,
  values,
} from 'ramda';
import { classify } from 'inflection';

import { ascendString } from '../../util/string';
import { PGDBContext } from './index';
import { Schema } from '../../schema';
import { Type } from '../../schema/type';
import { Field } from '../../schema/field';
import { Index } from '../../schema/idx';
import { Relation } from '../../schema/relation';

import {
  SQLWithValues,
  addColumnSql,
  addIndexSql,
  alterColumnDefaultSql,
  alterColumnNullabilitySql,
  alterColumnTypeSql,
  createJoinTableSql,
  createTableSql,
  dropAllTablesSql,
  dropColumnSql,
  dropConstraintSql,
  dropIndexSql,
  dropTableSql,
  joinTableMetadataSql,
  renameRelationForeignKeySql,
  renameTableSql,
  tableIndexMetadataSql,
  tableMetadataSql,
} from './sql';
import { execStmt, fieldFromDBMetadata } from './index';
import { createRelationType } from '../../schema/relation';
import { merge } from '../../util/object';

export const indexFromDBMetadata = curry((parentType: Type, row: any): Index => ({
  parentType,
  name: row.indexname,
  columns: split(/\s*,\s*/, row.columns),
  primary: row.primary,
  unique: row.unique,
  type: row.method,
}));

export const relationsFromDBMetadata = async (dbCtx: PGDBContext, types: Type[]): Promise<Relation[]> => {
  const { rows } = await execStmt(dbCtx.client, joinTableMetadataSql(dbCtx));
  const relationsHash = reduce((relations, row: any) => {
    const tableName = row.table_name;
    const name = `${classify(tableName)}`;
    const type = find(propEq('name', classify(row.foreign_table_name)), types);
    if (!type) return relations;
    const relation = {
      [name]: { name, tableName, types: [type] },
    };
    return merge(relations, relation);
  }, {}, rows);
  const byName = ascendString(prop('name'));
  const relations = values(relationsHash);
  return map((r: Relation) => assoc('types', sort(byName, r.types), r), relations);
};

export const parseDBSchema = async (dbCtx: PGDBContext): Promise<Schema> => {
  const { rows } = await execStmt(dbCtx.client, tableMetadataSql(dbCtx));

  const metadata = reduce((types: any, row: any) => {
    if (row.table_name === '__migrations') return types;
    if (!types[row.table_name]) {
      types[row.table_name] = {
        name: classify(row.table_name),
        tableName: row.table_name,
      };
    }
    const field = fieldFromDBMetadata(types[row.table_name], row);
    const fieldMeta = { fields: [field] };
    const typeWithFields = merge(types[row.table_name], fieldMeta);
    const updatedTypes = merge(types, { [row.table_name]: typeWithFields });
    return updatedTypes;
  }, {}, rows);

  // add indexes
  const types = await P.all(map(async (t: Type) => {
    const dbIndexes = (await execStmt(dbCtx.client, tableIndexMetadataSql(dbCtx, t.tableName))).rows;
    const indexes = map(indexFromDBMetadata(t), dbIndexes);
    map(idx => {
      if (length(idx.columns) !== 1) return null;
      const field = find(propEq('columnName', head(idx.columns)), t.fields);
      if (field) {
        field.primaryKey = idx.primary;
        field.unique = idx.unique;
      }
    }, indexes);
    return assoc('indexes', indexes, t);
  }, values(metadata)));

  const relations = await relationsFromDBMetadata(dbCtx, types);

  return {
    types,
    relations,
    enums: [],
  };
};

export const dropAllTables = async (dbCtx: PGDBContext): Promise<any> => {
  try {
    return await execStmt(dbCtx.client, dropAllTablesSql(dbCtx));
  } catch (e) {
    if (!e.message.match(/schema.*does not exist/)) {
      throw e;
    }
  }
};

export const colSpec = (f: Field): string =>
  `${f.columnName} ${f.sqlType} ${f.size > 0 ? `(${f.size})` : ''} ${f.nullable ? '' : 'not null'} ${f.unique ? 'unique' : ''} ${f.primaryKey ? 'primary key' : ''}`;

export const createTable = curry(async (dbCtx: PGDBContext, type: Type): Promise<any> =>
  execStmt(dbCtx.client, createTableSql(dbCtx, type)));

export const createJoinTable = curry(async (dbCtx: PGDBContext, relation: Relation): Promise<any> =>
  execStmt(dbCtx.client, createJoinTableSql(dbCtx, relation)));

export const createTableFromRelation = curry(async (
  dbCtx: PGDBContext,
  relation: Relation,
): Promise<any> => {
  const type = createRelationType(relation);
  return execStmt(dbCtx.client, createTableSql(dbCtx, type));
});

export const createTables = curry(async (dbCtx: PGDBContext, schema: Schema): Promise<any[]> =>
  P.all(map(createTable(dbCtx), schema.types)));

export const createTablesFromTypes = curry(async (
  dbCtx: PGDBContext,
  types: Type[],
): Promise<any[]> =>
  P.all(map(createTable(dbCtx), types)));

export const createJoinTables = curry(async (
  dbCtx: PGDBContext,
  relations: Relation[],
): Promise<any[]> =>
  P.all(map(createJoinTable(dbCtx), relations)));

export const createTablesFromRelations = curry(async (
  dbCtx: PGDBContext,
  relations: Relation[],
): Promise<any[]> =>
  P.all(map(createTableFromRelation(dbCtx), relations)));

export const renameTable = async (
  dbCtx: PGDBContext,
  oldName: string,
  newName: string,
): Promise<QueryResult> =>
  execStmt(dbCtx.client, renameTableSql(dbCtx, oldName, newName));

export const renameRelationForeignKeys = async (
  dbCtx: PGDBContext,
  oldRelation: Relation,
  newRelation: Relation,
): Promise<any[]> => {
  const p1 = execStmt(
    dbCtx.client,
    renameRelationForeignKeySql(dbCtx, oldRelation, newRelation, newRelation.srcType),
  );
  const p2 = execStmt(
    dbCtx.client,
    renameRelationForeignKeySql(dbCtx, oldRelation, newRelation, newRelation.targetType),
  );
  return P.all([p1, p2]);
};

export const addColumn = curry(async (dbCtx: PGDBContext, field: Field): Promise<any> =>
  execStmt(dbCtx.client, addColumnSql(dbCtx, field)));

export const addColumns = curry(async (dbCtx: PGDBContext, fields: Field[]): Promise<any[]> =>
  P.all(map(addColumn(dbCtx), fields)));

export const addIndex = curry(async (dbCtx: PGDBContext, index: Index): Promise<any> =>
  execStmt(dbCtx.client, addIndexSql(dbCtx, index)));

export const addIndexes = curry(async (dbCtx: PGDBContext, indexes: Index[]): Promise<any[]> =>
  P.all(map(addIndex(dbCtx), indexes)));

export const updateColumn = curry(async (dbCtx: PGDBContext, update: any): Promise<any> => {
  let updates: SQLWithValues[] = [];
  const { field } = update;

  if (contains('type', update.changes)) {
    const typeSql = alterColumnTypeSql(dbCtx, field);
    updates = append(typeSql, updates);
  }

  if (contains('default', update.changes)) {
    const defaultSql = alterColumnDefaultSql(dbCtx, field);
    updates = append(defaultSql, updates);
  }

  if (contains('nullability', update.changes)) {
    const nullabilitySql = alterColumnNullabilitySql(dbCtx, field);
    updates = append(nullabilitySql, updates);
  }

  return P.all(map(sql => execStmt(dbCtx.client, sql), updates));
});

export const updateColumns = curry(async (dbCtx: PGDBContext, updates: any[]): Promise<any[]> =>
  P.all(map(updateColumn(dbCtx), updates)));

export const dropColumn = curry(async (dbCtx: PGDBContext, field: Field): Promise<any> =>
  execStmt(dbCtx.client, dropColumnSql(dbCtx, field)));

export const dropColumns = curry(async (dbCtx: PGDBContext, fields: Field[]): Promise<any[]> =>
  P.all(map(dropColumn(dbCtx), fields)));

export const dropTable = curry(async (dbCtx: PGDBContext, type: Type): Promise<QueryResult> =>
  execStmt(dbCtx.client, dropTableSql(dbCtx, type)));

export const dropTables = curry(async (dbCtx: PGDBContext, types: Type[]): Promise<any[]> =>
  P.all(map(dropTable(dbCtx), types)));

export const dropConstraint = curry(async (
  dbCtx: PGDBContext,
  type: Type,
  constraintName: string,
): Promise<any> =>
  execStmt(dbCtx.client, dropConstraintSql(dbCtx, type, constraintName)));

export const dropIndex = curry(async (dbCtx: PGDBContext, index: Index): Promise<any[]> =>
  ((index.primary || index.unique)
    ? dropConstraint(dbCtx, index.parentType, index.name)
    : execStmt(dbCtx.client, dropIndexSql(dbCtx, index))));

export const dropIndexes = curry(async (dbCtx: PGDBContext, indexes: Index[]): Promise<any[]> =>
  P.all(map(dropIndex(dbCtx), indexes)));
