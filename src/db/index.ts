// @flow

import { FutureInstance } from 'fluture';
import { Schema } from '../schema';
import { Type } from '../schema/type';
import { Field } from '../schema/field';
import { Relation } from '../schema/relation';

export type SqlType = {
  sqlType: string,
  size: number,
};

export type DB = {
  parseDBSchema: () => Promise<any>,
  dropAllTables: () => Promise<any>,
  tableNames: () => Promise<string[]>,
  createTables: (schema: Schema) => Promise<any>,
  migrate: (typedefs: string, schema: Schema) => FutureInstance<{}, [boolean, string]>,
  sqlType: (gqlType: string) => SqlType,
  findOne: (type: Type, where: any) => Promise<any>,
  findOneInRelation: (srcId: string, srcField: Field, targetType: Type, relation: Relation) => Promise<any>,
  findManyInRelation: (srcId: string, srcField: Field, targetType: Type, relation: Relation) => Promise<any>,
  findThrough: (relation: Relation, srcIds: string[], where?: any, orderBy?: any, skip?: number, after?: string, before?: string, first?: number, last?: number) => Promise<any>,
  findOneThrough: (relation: Relation, srcIds: string[]) => Promise<any>,
  find: (type: Type, where: any, orderBy: any, skip: number, after: string, before: string, first: number, last: number) => Promise<any>,
  count: (type: Type, where: any, orderBy: any, skip: number, after: string, before: string, first: number, last: number) => Promise<any>,
  insert: (type: Type, data: any) => Promise<any>,
  update: (type: Type, data: any, where: any) => Promise<any>,
  upsert: (type: Type, where: any, create: any, update: any) => Promise<any>,
  del: (type: Type, where: any) => Promise<any>,
  // updateMany: (type: Type, data: any, where: any) => Promise<any>,
  delMany: (type: Type, where: any) => Promise<any>,
  sql: (sql: string, values: any[]) => Promise<any>,
};
