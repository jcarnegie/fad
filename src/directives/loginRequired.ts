import { contains } from 'ramda';

export default (directiveContext: any, originalResolver: any) =>
  async (source: any, args: any, context: any, info: any) => {
    // if the target is a field then we just return null if they're not logged in
    const isLoggedIn = !!context.user;
    const targetIsField = !!directiveContext.field;
    if (!isLoggedIn && targetIsField) return null;

    // Target is the type. Handle CRUD resolvers + applyTo here
    const { directive, fieldName } = directiveContext;
    const doesApply = !directive.args.applyTo || contains(fieldName, directive.args.applyTo);
    if (doesApply && !isLoggedIn) throw new Error('Login required');

    // Otherwise, call the original resolver
    return originalResolver(source, args, context, info);
  };