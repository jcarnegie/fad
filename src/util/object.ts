// @flow
import {
  adjust,
  append,
  concat,
  contains,
  curry,
  forEachObjIndexed,
  fromPairs,
  keys,
  map,
  mergeWith,
  prop,
  propSatisfies,
  reduce,
  uniq,
  toPairs,
} from 'ramda';

// Taken from here: https://pastel.li/2017/ramda-deep-merge-objects
const mergePlan = (x: any, y: any): any => {
  if (Array.isArray(x) && Array.isArray(y)) {
    return uniq(concat(x, y));
  }

  if (typeof x === 'object' && typeof y === 'object') {
    return mergeWith(mergePlan, x, y);
  }

  return y;
};

export const merge = mergeWith(mergePlan);

export const propIn = curry((property: string, values: [string], object: any): boolean =>
  propSatisfies(p => contains(p, values), property, object));

export const propContains = curry((property: string, value: any, object: any) =>
  propSatisfies(p => contains(value, p), property, object));

// Todo: better typing
// Todo: handle null returned by prop, or scope sortedKeys to the list of keys found in object
export const sortedValues = (sortedKeys: any[], object: any): any =>
  reduce((list: any, key) => append(prop(key, object), list), [], sortedKeys);

// Taken from here: https://github.com/ramda/ramda/wiki/Cookbook#whereall-sort-of-like-a-recursive-version-of-ramdas-where
export const whereAll = curry((spec: any, obj: any): boolean => {
  if (typeof obj === 'undefined') {
    if (spec === null || typeof spec === 'boolean') {
      return !spec;
    }
    return false;
  } else if (spec === false) {
    return false;
  }
  let output = true;
  forEachObjIndexed((v, k) => {
    if (v === null || typeof v === 'boolean' || keys(v).length) {
      if (!whereAll(v, obj[k])) {
        output = false;
      }
    } else if (!v(obj[k])) {
      output = false;
    }
  }, spec);
  return output;
});

// Taken from here: https://github.com/ramda/ramda/wiki/Cookbook#map-keys-of-an-object-rename-keys-by-a-function
export const mapKeys = curry((fn, obj) =>
  fromPairs(map(adjust(0, fn), toPairs(obj))));