import { camelize, pluralize } from 'inflection';
import { compose, curry, head, isNil, length, map, not, reduce } from 'ramda';
import { Schema } from '../schema';
import { Type } from '../schema/type';
import { DB } from '../db';
import { relationFields, Field } from '../schema/field';
import { Relation, findRelationByFieldName, RELATION_TYPE_HAS_ONE } from '../schema/relation';

const notNil = compose(not, isNil);

/**
 * Support things like:
 * 
 * api.users(*filters*)
 * api.user(id)
 * api.createUser(*data*)
 * api.updateUser(*data*)
 * api.upsertUser(*data*)
 * api.deleteUser(*data*)
 * api.updateManyUsers()
 * api.deleteManyUsers()
 * 
 * api.users({ where: { createdAt_gt: <timestamp> }, include: {
 *   posts: { where: { published: true }, include: ['comments']
 * 
 *   }}
 * }})
 */

const findOne = curry((db: DB, type: Type, where: any) =>
    db.findOne(type, where));

const find = curry((
    db: DB,
    type: Type,
    where: any,
    orderBy: any,
    skip: number,
    after: string,
    before: string,
    first: number,
    last: number
) => db.find(type, where, orderBy, skip, after, before, first, last));

const findConnection = curry(async (
    db: DB,
    type: Type,
    where: any,
    orderBy: any,
    skip: number,
    after: string,
    before: string,
    first: number,
    last: number
) => {
    const edges = await db.find(type, where, orderBy, skip, after, before, first, last);
    let count = 0;
    if (first || last) {
        count = await db.count(type, where, orderBy, skip, after, before, first, last);
    }
    const data = {
        pageInfo: {
            hasNextPage: notNil(first) && count > length(edges),
            hasPreviousPage: notNil(last) && count > length(edges),
            startCursor: (edges[0] ? edges[0].id : null),
            endCursor: (edges[edges.length - 1] ? edges[edges.length - 1].id : null),
        },
        edges: map(e => ({ cursor: e.id, node: e }), edges),
    };
    return data;
});

const create = curry((db: DB, type: Type, data: any) =>
    db.insert(type, data));

const update = curry((db: DB, type: Type, data: any, where: any) =>
    db.update(type, data, where));

const upsert = curry((db: DB, type: Type, where: any, create: any, update: any) =>
    db.upsert(type, where, create, update));

const del = curry((db: DB, type: Type, where: any) => 
    db.del(type, where));

// const updateMany = curry((db: DB, type: Type, data: any, where: any) =>
//     db.updateMany(type, data, where));

const delMany = curry((db: DB, type: Type, where: any) =>
    db.delMany(type, where));

export const makeApi = (db: DB, schema: Schema): any => {
    const relationApiReducer = curry((schema: Schema, type: Type, api: any, field: Field) => {
        const relation = findRelationByFieldName(schema, type, field.name);
        const typeName = camelize(type.name, true);
        const isHasOne = relation.relationType === RELATION_TYPE_HAS_ONE;
        const fieldName = camelize(field.name);
        const name = `${typeName}${fieldName}`;
        const fn = isHasOne
            ? async (id: string) => (await db.findOneThrough(relation, [id]))[0]
            : async (
                srcIds: string[],
                where?: any,
                orderBy?: any,
                skip?: number,
                after?: string,
                before?: string,
                first?: number,
                last?: number
            ) => (await db.findThrough(relation, srcIds, where, orderBy, skip, after, before, first, last))[0];
        return {
            ...api,
            [name]: fn,
        };
    });

    const reducer = (api: any, type: Type) => {
        const typeRelationFields = relationFields(type);
        const relationApi = reduce(relationApiReducer(schema, type), {}, typeRelationFields);
        return {
            ...api,
            // queries
            [camelize(type.name, true)]: findOne(db, type),
            [pluralize(camelize(type.name, true))]: find(db, type),
            [`${pluralize(camelize(type.name, true))}Connection`]: findConnection(db, type),
            // mutations
            [`create${type.name}`]: create(db, type),
            [`update${type.name}`]: update(db, type),
            [`upsert${type.name}`]: upsert(db, type),
            [`delete${type.name}`]: del(db, type),
            // [`updateMany${pluralize(type.name)}`]: updateMany(db, type),
            [`deleteMany${pluralize(type.name)}`]: delMany(db, type),
            ...relationApi,
        };
    };
    return reduce(reducer, {}, schema.types);
};