**trikata**

GraphQL framework for Node.js. Given a GraphQL schema, we'll enhance it and automatically generate resolvers for you. Cut down on your development time and focus on delivering the features that matter for your app.

- Get CRUD resolvers for each of your types for free
- Auth out of the box
- Data caching for high performance
- Extensible plugin architecture via NPM modules

*Get Started*
```shell
mkdir hello-trikata
cd hello-trikata
npm init -y
npm i -S trikata
node_modules/.bin/tk init
# make sure you have a postgres db setup and listening on port 5432 w/ pg user
# 'postgres' and password set to 'password'
# You can also set the PG_URL env var to point to the DB. Here's an example:
# postgresql://postgres:password@localhost:5432/postgres
npm run migrate
npm start
```