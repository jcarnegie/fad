import '@babel/polyfill';
import uuid from 'uuid/v4';
import sinon from 'sinon';
import { insert } from '../../../../../src/db/pg/index';

describe('Postgresql', () => {
  it('should insert data into a table', async () => {
    const data = { email: 'john@doe.com', name: 'John Doe' };
    const dbCtx = {
      schema: 'trikata',
      client: {
        query: sinon.stub().returns({
          rows: [{ ...data, id: 1 }],
        }),
      },
      idFn: uuid,
    };
    const type = {
      tableName: 'users',
      fields: [
        { name: 'id' },
        { name: 'name' },
        { name: 'email' },
      ],
    };

    const result = await insert(dbCtx, type, data);
    const q = 'insert into users (email, name) values ($1, $2);';
    const values = ['john@doe.com', 'John Doe'];
    expect(result).toEqual({ ...data, id: 1 });
    expect(dbCtx.client.query.calledWith(q, values));
  });
});
