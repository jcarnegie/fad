import { FutureInstance, promise } from 'fluture';
import { clone } from 'ramda';
import { MigrationActions, create } from '../../../src/migrations/plan';
import { createParseSchemaContext, ParseSchemaContext } from '../../../src/schema/context';
import { parseTypedefs } from '../../../src/schema';

const EMPTY_SCHEMA = {
  types: [],
  statelessTypes: [],
  queryType: null,
  mutationType: null,
  enums: [],
  relations: []
};

const BASIC_USER_SDL = `
type User {
  id: ID!
  email: String!
}
`;

const RENAMABLE_RELATION_SDL_1 = `
type User {
  id: ID!
  posts: [Post!]! @relation(name: "Test")
}
type Post { id: ID! }
`;

const RENAMABLE_RELATION_SDL_2 = `
type User {
  id: ID!
  posts: [Post!]!
}
type Post { id: ID! }
`;

const DB_MISSING_RELATION_SDL = `
type User {
  id: ID!
}
`;

const GQL_WITH_RELATION_SDL = RENAMABLE_RELATION_SDL_2;

const GQL_ADD_NAME_TO_USER = `
type User {
  id: ID!
  name: String!
  email: String!
}
`;

const GQL_ADD_EMAIL_INDEX_TO_USER = `
type User {
  id: ID!
  email: String! @index
}
`;

const GQL_MAKE_EMAIL_NULLABLE = `
type User {
  id: ID!
  email: String
}
`;

const GQL_SET_EMAIL_DEFAULT = `
type User {
  id: ID!
  email: String! @default(value: "test@acme.com")
}
`;

const GQL_SET_EMAIL_DEFAULT_AND_MAKE_NULLABLE = `
type User {
  id: ID!
  email: String @default(value: "test@acme.com")
}
`;

const GQL_REMOVE_EMAIL = `type User { id: ID! }`;

const USER_AND_POST_SDL = `
type User { id: ID! }
type Post { id: ID! }
`;

const GQL_USER = `type User { id: ID! }`;

describe('Migration Plan', () => {
  let parseSchemaContext = null;
  let schema = null;
  let dbSchema = null;
  let type = null;

  beforeEach(async () => {
    parseSchemaContext = await promise(
      <FutureInstance<Error, ParseSchemaContext>>createParseSchemaContext()
    );
    schema = clone(EMPTY_SCHEMA);
    dbSchema = clone(EMPTY_SCHEMA);
  });

  it('should create a migration plan', () => {
    const plan = create(schema, dbSchema);
    expect(plan).toBeDefined();
  });

  it('should create an empty plan when both schemas are empty', () => {
    const plan = create(schema, dbSchema);
    expect(plan).toEqual({ actions: [] });
  });

  it('should add a create table action to the plan', () => {
    const schema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const plan = create(schema, dbSchema);
    const action = plan.actions[0];
    expect(plan.actions).toHaveLength(1);
    expect(action).toEqual({
      actionType: MigrationActions.CREATE_TABLE,
      type: schema.types[0],
    });
  });

  it('should add a rename relation tables action to the plan', () => {
    const dbSchema = parseTypedefs(parseSchemaContext, RENAMABLE_RELATION_SDL_1);
    const schema = parseTypedefs(parseSchemaContext, RENAMABLE_RELATION_SDL_2);
    const plan = create(schema, dbSchema);
    const action = plan.actions[0];
    expect(plan.actions).toHaveLength(1);
    expect(action).toEqual({
      actionType: MigrationActions.RENAME_RELATION_TABLE,
      new: schema.relations[0],
      old: dbSchema.relations[0],
    });
  });

  it('should add an add relation tables action to the plan', () => {
      const dbSchema = parseTypedefs(parseSchemaContext, DB_MISSING_RELATION_SDL);
      const schema = parseTypedefs(parseSchemaContext, GQL_WITH_RELATION_SDL);
      const plan = create(schema, dbSchema);
      const addRelationTableAction = plan.actions[1];
      expect(plan.actions).toHaveLength(2);
      expect(addRelationTableAction).toEqual({
        actionType: MigrationActions.CREATE_RELATION_TABLE,
        relation: schema.relations[0],
      });
  });

  it('should add an add new columns action to the plan', () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_ADD_NAME_TO_USER);
    const plan = create(schema, dbSchema);
    const addRelationTableAction = plan.actions[0];
    expect(plan.actions).toHaveLength(1);
    expect(addRelationTableAction).toEqual({
      actionType: MigrationActions.ADD_COLUMN,
      field: schema.types[0].fields[1],
    });
  });

  it('should add an add new indexes action to the plan', () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_ADD_EMAIL_INDEX_TO_USER);
    const plan = create(schema, dbSchema);
    expect(plan.actions).toHaveLength(1);
    expect(plan.actions[0]).toEqual({
      actionType: MigrationActions.ADD_INDEX,
      index: schema.types[0].indexes[1],
    });
  });

  it('should add an update columns (nullability) action to the plan', () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_MAKE_EMAIL_NULLABLE);
    const plan = create(schema, dbSchema);
    expect(plan.actions).toHaveLength(1);
    expect(plan.actions[0]).toEqual({
      actionType: MigrationActions.UPDATE_COLUMN,
      changes: ['nullability'],
      field: schema.types[0].fields[1],
      dbField: dbSchema.types[0].fields[1],
    });
  });

  it('should add an update columns (default) action to the plan', () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_SET_EMAIL_DEFAULT);
    const plan = create(schema, dbSchema);
    expect(plan.actions).toHaveLength(1);
    expect(plan.actions[0]).toEqual({
      actionType: MigrationActions.UPDATE_COLUMN,
      changes: ['default'],
      field: schema.types[0].fields[1],
      dbField: dbSchema.types[0].fields[1]
    });
  });

  it('should add an update columns (default + nullability) action to the plan', () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_SET_EMAIL_DEFAULT_AND_MAKE_NULLABLE);
    const plan = create(schema, dbSchema);
    expect(plan.actions).toHaveLength(1);
    expect(plan.actions[0]).toEqual({
      actionType: MigrationActions.UPDATE_COLUMN,
      changes: ['nullability', 'default'],
      field: schema.types[0].fields[1],
      dbField: dbSchema.types[0].fields[1]
    });
  });

  it('should add a remove columns action to the plan', () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_REMOVE_EMAIL);
    const plan = create(schema, dbSchema);
    expect(plan.actions).toHaveLength(1);
    expect(plan.actions[0]).toEqual({
      actionType: MigrationActions.REMOVE_COLUMN,
      field: dbSchema.types[0].fields[1],
    });
  });

  it('should add a remove tables action to the plan', () => {
    const dbSchema = parseTypedefs(parseSchemaContext, USER_AND_POST_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_USER);
    const plan = create(schema, dbSchema);
    expect(plan.actions).toHaveLength(1);
    expect(plan.actions[0]).toEqual({
      actionType: MigrationActions.REMOVE_TABLE,
      type: dbSchema.types[1]
    });
  });

  it('should add a remove indexes action to the plan', () => {
    const dbSchema = parseTypedefs(parseSchemaContext, GQL_ADD_EMAIL_INDEX_TO_USER);
    const schema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const plan = create(schema, dbSchema);
    expect(plan.actions).toHaveLength(1);
    expect(plan.actions[0]).toEqual({
      actionType: MigrationActions.REMOVE_INDEX,
      index: dbSchema.types[0].indexes[1],
    });
  });
});