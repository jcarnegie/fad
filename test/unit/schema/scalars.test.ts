import { fork } from 'fluture';
import { loadScalarTypes } from "../../../src/schema/scalars";

describe('Scalar types', () => {
    it('should load a scalar type from the file system', (done) => {
        const reject = done;

        const resolve = scalars => {
          expect(scalars[0].name).toEqual("JSON");
          expect(scalars[0].resolver).toBeDefined;
          expect(scalars[1].name).toEqual("JSON");
          expect(scalars[1].resolver).toBeDefined;
          done();
        };
        const eventualScalars = loadScalarTypes(['test/unit/fixtures/scalars', 'test/unit/fixtures/scalars']);
        fork(reject)(resolve)(eventualScalars);
    });
});