import { map, promise } from "fluture";
import { identity } from 'ramda';
import { enhance } from '../../../src/schema/enhance';
import { createParseSchemaContext } from '../../../src/schema/context';

describe('Enhance Typedefs', () => {
  const typedefs = `
    type User {
      id: ID!
      email: String!
      name: String
    }
  `;

  let context = null;

  beforeEach(() => {
    return promise(map(parseContext => context = parseContext)(createParseSchemaContext()));
  });

  it('should add a createUser field to the typedefs', () => {
    const enhanced = enhance(context, typedefs);
    expect(enhanced).toMatch('createUser(data: UserCreateInput!): User!');
  });

  it('should add a updateUser field to the typedefs', () => {
    const enhanced = enhance(context, typedefs);
    expect(enhanced).toMatch('updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User');
  });

  it('should add a deleteUser field to the typedefs', () => {
    const enhanced = enhance(context, typedefs);
    expect(enhanced).toMatch('deleteUser(where: UserWhereUniqueInput!): User');
  });

  it('should add a updateManyUsers field to the typedefs', () => {
    const enhanced = enhance(context, typedefs);
    expect(enhanced).toMatch('updateManyUsers(data: UserUpdateInput!, where: UserWhereInput!): BatchPayload!');
  });

  it('should add a deleteManyUsers field to the typedefs', () => {
    const enhanced = enhance(context, typedefs);
    expect(enhanced).toMatch('deleteManyUsers(where: UserWhereInput!): BatchPayload!');
  });

  it('should add a user field to the typedefs', () => {
    const enhanced = enhance(context, typedefs);
    expect(enhanced).toMatch('user(where: UserWhereUniqueInput!): User');
  });

  it('should add a users field to the typedefs', () => {
    const enhanced = enhance(context, typedefs);
    expect(enhanced).toMatch('where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int');
  });
});
