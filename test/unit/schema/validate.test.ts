import { validate } from '../../../src/schema/validate';

describe('@validate Directive', () => {
  let type = null;
  let data = null;

  beforeEach(() => {
    type = {
      name: 'User',
      fields: [{
        parentType: { name: 'User' },
        name: 'email',
        directives: [{
          name: 'validate',
          args: {
            format: 'email',
          },
        }]
      }, {
          parentType: { name: 'User' },
          name: 'phone',
          directives: [{
            name: 'validate',
            args: {
              format: 'phone',
            },
          }]
        }]
    }
    data = { email: 'sam@acme.com', phone: '+14155551212' };
  });

  it('should validate an email address', () => {
    expect(validate(type, data)).toBeTruthy();
  });

  it('should validate a null email address', () => {
    expect(validate(type, { email: null }));
  })

  it('should reject an invalid email address', () => {
    data.email = 'foo@';
    expect.assertions(1);
    try {
      validate(type, data)
    } catch (e) {
      expect(e.message).toEqual('User.email is not a valid email address');
    }
  });

  it('should validate a phone number', () => {
    expect(validate(type, data)).toBeTruthy();
  });

  it('should reject an invalid phone number', () => {
    data.phone = 'foo@';
    expect.assertions(1);
    try {
      validate(type, data);
    } catch (e) {
      expect(e.message).toEqual('User.phone is not a valid phone number');
    }
  });
});