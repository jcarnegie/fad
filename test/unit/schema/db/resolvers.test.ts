import { fork } from 'fluture';
import { loadCustomResolvers } from '../../../../src/schema/resolvers';

describe('Resolvers', () => {
    describe('loadCustomResolvers', () => {
        it('should load custom resolvers', (done) => {
            const reject = done;
            const resolve = resolvers => {
                expect(resolvers.Query).toHaveProperty('findUser');
                expect(resolvers.Mutation).toHaveProperty('createUser');
                done();
            }; 
            const future = loadCustomResolvers(['test/unit/fixtures/resolvers']);
            fork(reject)(resolve)(future);
        });
    })
})