
addresses: [Address]

{ kind: 'FieldDefinition',
  description: undefined,
  name:
   { kind: 'Name', value: 'addresses', loc: { start: 81, end: 90 } },
  arguments: [],
  type:
   { kind: 'ListType',
     type:
      { kind: 'NamedType',
        name:
         { kind: 'Name', value: 'Address', loc: { start: 93, end: 100 } },
        loc: { start: 93, end: 100 } },
     loc: { start: 92, end: 101 } },
  directives: [],
  loc: { start: 81, end: 101 } }

isListType: whereEq({ type: { kind: 'ListType' } }, field)
isListNullable: whereEq({ type: { kind: 'ListType', type: { kind: 'NamedType' } } }, field);
listElementsNullable: isListType(field) && pathEq(['type', 'type', 'kind', 'NamedType'])
fieldType: path(['type', 'type', 'name', 'value'], field);



addresses: [Address!]

{ kind: 'FieldDefinition',
  description: undefined,
  name:
   { kind: 'Name', value: 'addresses', loc: { start: 81, end: 90 } },
  arguments: [],
  type:
   { kind: 'ListType',
     type:
      { kind: 'NonNullType',
        type:
         { kind: 'NamedType',
           name:
            { kind: 'Name', value: 'Address', loc: { start: 93, end: 100 } },
           loc: { start: 93, end: 100 } },
        loc: { start: 93, end: 101 } },
     loc: { start: 92, end: 102 } },
  directives: [],
  loc: { start: 81, end: 102 } }

isListType: whereEq({ type: { kind: 'ListType' } }, field)
isListNullable: whereEq({ type: { kind: 'ListType', type: { kind: 'NamedType' } } }, field);
listElementsNullable: isListType(field) && pathEq(['type', 'type', 'kind', 'NamedType'])
fieldType: path(['type', 'type', 'type', 'name', 'value'], field);



addresses: [Address]!

{ kind: 'FieldDefinition',
  description: undefined,
  name:
   { kind: 'Name', value: 'addresses', loc: { start: 81, end: 90 } },
  arguments: [],
  type:
   { kind: 'NonNullType',
     type:
      { kind: 'ListType',
        type:
         { kind: 'NamedType',
           name:
            { kind: 'Name', value: 'Address', loc: { start: 93, end: 100 } },
           loc: { start: 93, end: 100 } },
        loc: { start: 92, end: 101 } },
     loc: { start: 92, end: 102 } },
  directives: [],
  loc: { start: 81, end: 102 } }

isListType: whereEq({ type: { type: { kind: 'ListType' } } }, field)
isListNullable: isListType(field) && whereEq({ type: { kind: 'ListType' } }, field);
listElementsNullable: whereEq({ type: { type: { kind: 'ListType', type: { kind: 'NamedType' } } } }, field)
fieldType: path(['type', 'type', 'type', 'name', 'value'], field);



addresses: [Address!]!

{ kind: 'FieldDefinition',
  description: undefined,
  name:
   { kind: 'Name', value: 'addresses', loc: { start: 81, end: 90 } },
  arguments: [],
  type:
   { kind: 'NonNullType',
     type:
      { kind: 'ListType',
        type:
         { kind: 'NonNullType',
           type:
            { kind: 'NamedType',
              name: { kind: 'Name', value: 'Address', loc: [Object] },
              loc: { start: 93, end: 100 } },
           loc: { start: 93, end: 101 } },
        loc: { start: 92, end: 102 } },
     loc: { start: 92, end: 103 } },
  directives: [],
  loc: { start: 81, end: 103 } }

isListType: pathEq(['type', 'type', 'kind'], 'ListType', field)
isListNullable: isListType(field) && pathEq(['type', 'type', 'type', 'kind'], 'NamedType')
listElementsNullable: isListType(field) && pathEq(['type', 'type', 'type', 'kind', 'NamedType'])
fieldType: path(['type', 'type', 'type', 'type', 'name', 'value'], field);
