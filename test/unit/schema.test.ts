import { map, promise } from "fluture";
import { identity, length } from 'ramda';
import { parseTypedefs } from '../../src/schema';
import { createParseSchemaContext } from '../../src/schema/context';

describe('GraphQL Parse Typedefs', () => {
  let typedefs = `
      type User {
        id: ID!
        email: String!
        phone: String
      }
    `;

  let schema = null;

  beforeEach(() => {
    const parse = parseContext => {
      schema = parseTypedefs(parseContext, typedefs);
    };
    return promise(map(parse)(createParseSchemaContext()));
  });

  it('should produce a type object', () => {
    expect(length(schema.types || [])).toEqual(1);
    expect(schema.types[0].name).toEqual('User');
    expect(length(schema.types[0].fields || [])).toEqual(5);
  });

  describe('Fields', () => {
    it('should mark non-null fields', () => {
      expect(schema.types[0].fields[0].nullable).toEqual(false);
    });

    it('should mark nullable fields', () => {
      expect(schema.types[0].fields[2].nullable).toEqual(true);
    });

    it('should make ID fields primary keys', () => {
      expect(schema.types[0].fields[0].primaryKey).toEqual(true);
    });

    it('shoule set the parentType to the type', () => {
      const type = schema.types[0];
      expect(type.fields[0].parentType).toEqual(type);
    });
  });

  describe('Stateless', () => {
    typedefs = `
      type User {
        id: ID!
        email: String!
        phone: String
      }

      type Foo @stateless {
        a: String
      }
    `;

    beforeEach(() => {
      const parse = parseContext => {
        schema = parseTypedefs(parseContext, typedefs);
      };
      return promise(map(parse)(createParseSchemaContext()));
    });

    it("shouldn't include the stateless type in the schema types list", () => {
      expect(length(schema.types || [])).toEqual(1);
      expect(schema.types[0].name).toEqual('User');
    });

    it("should include the stateless type in the schema stateless types list", () => {
      expect(length(schema.statelessTypes || [])).toEqual(1);
      expect(schema.statelessTypes[0].name).toEqual('Foo');
    });
  });
});
