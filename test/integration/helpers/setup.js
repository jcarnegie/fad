require('@babel/polyfill');
const { start } = require('./db');

// eslint-disable-next-line func-names
module.exports = async function () {
  try {
    await start();
  } catch (e) {
    console.log('Error in global setup:', e);
  }
};
