module.exports = {
  globalSetup: '<rootDir>/helpers/setup.js',
  globalTeardown: '<rootDir>/helpers/teardown.js',
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
  ],
  setupTestFrameworkScriptFile: '<rootDir>/helpers/setupTest.js',
  testEnvironment: '<rootDir>/helpers/db-jest-environment.js',
  testMatch: [
    '**/__tests__/**/*.(j|t)s?(x)',
    '**/?(*.)+(spec|test).(j|t)s?(x)',
  ],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': 'babel-jest',
  },
};
