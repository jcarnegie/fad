Feature: Directives

  Tests for type and field directives

  Scenario: Enhance typedefs via unique field directive
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        email: String! @unique
        name: String!
      }      
      """
    When the schema is enhanced
    Then the enhanced schema should contain general definitions
      """
      input UserWhereUniqueInput {
        id: ID
        email: String
      }
      """