Feature: Has Many Relation Query

  Make sure resolvers return nested relations.

  Scenario: Has many relations
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
      | 2  | Jane Doe | jad@acme.com | true   |
    And rows in the addresses table
      | id | street      | city        | state | zip   |
      | 1  | 123 Main St | New York    | NY    | 11354 |
      | 2  | 123 Main St | Los Angeles | CA    | 90012 |
      | 3  | 123 Main St | Chicago     | IL    | 60202 |
    And rows in the _address_to_users table
      | id | user_id | address_id |
      | 1  | 1       | 1          |
      | 2  | 1       | 2          |
      | 3  | 2       | 3          |
    When a GraphQL query is executed
      """
        query {
          users {
            id name email active addresses {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [{
            "id": "1",
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": [{
              "id": "1",
              "street": "123 Main St",
              "city": "New York",
              "state": "NY",
              "zip": "11354"
            }, {
              "id": "2",
              "street": "123 Main St",
              "city": "Los Angeles",
              "state": "CA",
              "zip": "90012"
            }]
          }, {
            "id": "2",
            "name": "Jane Doe",
            "email": "jad@acme.com",
            "active": true,
            "addresses": [{
              "id": "3",
              "street": "123 Main St",
              "city": "Chicago",
              "state": "IL",
              "zip": "60202"
            }]
          }]
        }
      }
      """

  Scenario: Query with has many nested filtered query
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
      | 2  | Jane Doe | jad@acme.com | true   |
    And rows in the addresses table
      | id | name     | street      | city        | state | zip   |
      | 1  | Office   | 123 Main St | New York    | NY    | 11354 |
      | 2  | Office 2 | 123 Main St | Los Angeles | CA    | 90012 |
      | 3  | Home     | 123 Main St | Chicago     | IL    | 60202 |
    And rows in the _address_to_users table
      | id | user_id | address_id |
      | 1  | 1       | 1          |
      | 2  | 2       | 2          |
      | 3  | 1       | 3          |
    When a GraphQL query is executed
      """
        query {
          users {
            id name email active
            addresses(where: { name: "Home" }) {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [{
            "id": "1",
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": [{
              "id": "3",
              "street": "123 Main St",
              "city": "Chicago",
              "state": "IL",
              "zip": "60202"
            }]
          }, {
            "id": "2",
            "name": "Jane Doe",
            "email": "jad@acme.com",
            "active": true,
            "addresses": []
          }]
        }
      }
      """

  Scenario: Query with has many nested filtered query (order by)
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
      | 2  | Jane Doe | jad@acme.com | true   |
    And rows in the addresses table
      | id | name     | street      | city        | state | zip   |
      | 1  | Office   | 123 Main St | New York    | NY    | 11354 |
      | 2  | Office 2 | 123 Main St | Los Angeles | CA    | 90012 |
      | 3  | Home     | 123 Main St | Chicago     | IL    | 60202 |
    And rows in the _address_to_users table
      | id | user_id | address_id |
      | 1  | 1       | 1          |
      | 2  | 2       | 2          |
      | 3  | 1       | 3          |
    When a GraphQL query is executed
      """
        query {
          users {
            id name email active
            addresses(orderBy: name_ASC) {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [{
            "id": "1",
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": [{
              "id": "3",
              "name": "Home",
              "street": "123 Main St",
              "city": "Chicago",
              "state": "IL",
              "zip": "60202"
            }, {
              "id": "1",
              "name": "Office",
              "street": "123 Main St",
              "city": "New York",
              "state": "NY",
              "zip": "11354"
            }]
          }, {
            "id": "2",
            "name": "Jane Doe",
            "email": "jad@acme.com",
            "active": true,
            "addresses": [{
              "id": "2",
              "name": "Office 2",
              "street": "123 Main St",
              "city": "Los Angeles",
              "state": "CA",
              "zip": "90012"
            }]
          }]
        }
      }
      """