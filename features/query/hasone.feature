Feature: Has One Relation Query

  Make sure resolvers return nested relations.

  Scenario: Has one relation
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name      | email        | active |
      | 1  | John Doe  | jd@acme.com  | false  |
    And rows in the addresses table
      | id | street      | city     | state     | zip   |
      | 1  | 123 Main St | New York | New York  | 11354 |
    And rows in the _address_to_users table
      | id | user_id | address_id |
      | 1  | 1       | 1          |
    When a GraphQL query is executed
      """
        query {
          users {
            id name email active address {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1",
              "name": "John Doe",
              "email": "jd@acme.com",
              "active": false,
              "address": {
                "id": "1",
                "street": "123 Main St",
                "city": "New York",
                "state": "New York",
                "zip": "11354"
              }
            }
          ]
        }
      }
      """