Feature: Query Pagination

  Be able to specify SQL-like where clause elements via GraphQL

  Background:
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
      }
      """
    And rows in the users table
      | id | name      | email        | active |
      | 1  | John Doe  | jd@acme.com  | false  |
      | 2  | Jane Doe  | jad@acme.com | false  |
      | 3  | Joe Doe   | jod@acme.com | true   |
      | 4  | Chris Doe | cd@acme.com  | true   |
      | 5  | Anne Doe  | ad@acme.com  | true   |
      | 6  | Peter Doe | pd@acme.com  | true   |

  Scenario: First query filter
    When a GraphQL query is executed
      """
        query {
          users(first: 3) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false },
            { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: After query filter
    When a GraphQL query is executed
      """
        query {
          users(after: "4") {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: First + After query filter
    When a GraphQL query is executed
      """
        query {
          users(first: 2, after: "2") {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true },
            { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Last query filter
    When a GraphQL query is executed
      """
        query {
          users(last: 2) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Before query filter
    When a GraphQL query is executed
      """
        query {
          users(before: "3") {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false }
          ]
        }
      }
      """

  Scenario: Last + before query filter
    When a GraphQL query is executed
      """
        query {
          users(last: 2, before: "6") {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true },
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true }
          ]
        }
      }
      """