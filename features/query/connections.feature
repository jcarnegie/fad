Feature: Query Pagination

  Be able to specify SQL-like where clause elements via GraphQL

  Background:
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
      }
      """
    And rows in the users table
      | id | name      | email        | active |
      | 1  | John Doe  | jd@acme.com  | false  |
      | 2  | Jane Doe  | jad@acme.com | false  |
      | 3  | Joe Doe   | jod@acme.com | true   |
      | 4  | Chris Doe | cd@acme.com  | true   |
      | 5  | Anne Doe  | ad@acme.com  | true   |
      | 6  | Peter Doe | pd@acme.com  | true   |

  Scenario: Connection first query filter
    When a GraphQL query is executed
      """
        query {
          usersConnection(first: 3) {
            pageInfo { hasNextPage hasPreviousPage startCursor endCursor }
            edges { cursor node { id name email active } }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "usersConnection": {
            "pageInfo": {
              "hasNextPage": true,
              "hasPreviousPage": false,
              "startCursor": "1",
              "endCursor": "3"
            },
            "edges": [
              { "cursor": "1", "node": { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false } },
              { "cursor": "2", "node": { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false } },
              { "cursor": "3", "node": { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true } }
            ]
          }
        }
      }
      """

  Scenario: Connection after query filter
    When a GraphQL query is executed
      """
        query {
          usersConnection(after: "4") {
            pageInfo { hasNextPage hasPreviousPage startCursor endCursor }
            edges { cursor node { id name email active } }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "usersConnection": {
            "pageInfo": {
              "hasNextPage": false,
              "hasPreviousPage": false,
              "startCursor": "5",
              "endCursor": "6"
            },
            "edges": [
              { "cursor": "5", "node": { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true } },
              { "cursor": "6", "node": { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true } }
            ]
          }
        }
      }
      """

  Scenario: Connection first + after query filter
    When a GraphQL query is executed
      """
        query {
          usersConnection(first: 2, after: "2") {
            pageInfo { hasNextPage hasPreviousPage startCursor endCursor }
            edges { cursor node { id name email active } }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "usersConnection": {
            "pageInfo": {
              "hasNextPage": true,
              "hasPreviousPage": false,
              "startCursor": "3",
              "endCursor": "4"
            },
            "edges": [
              { "cursor": "3", "node": { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true } },
              { "cursor": "4", "node": { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true } }
            ]
          }
        }
      }
      """

  Scenario: Connection last query filter
    When a GraphQL query is executed
      """
        query {
          usersConnection(last: 2) {
            pageInfo { hasNextPage hasPreviousPage startCursor endCursor }
            edges { cursor node { id name email active } }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "usersConnection": {
            "pageInfo": {
              "hasNextPage": false,
              "hasPreviousPage": true,
              "startCursor": "5",
              "endCursor": "6"
            },
            "edges": [
              { "cursor": "5", "node": { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true } },
              { "cursor": "6", "node": { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true } }
            ]
          }
        }
      }
      """

  Scenario: Connection before query filter
    When a GraphQL query is executed
      """
        query {
          usersConnection(before: "3") {
            pageInfo { hasNextPage hasPreviousPage startCursor endCursor }
            edges { cursor node { id name email active } }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "usersConnection": {
            "pageInfo": {
              "hasNextPage": false,
              "hasPreviousPage": false,
              "startCursor": "1",
              "endCursor": "2"
            },
            "edges": [
              { "cursor": "1", "node": { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false } },
              { "cursor": "2", "node": { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false } }
            ]
          }
        }
      }
      """

  Scenario: Connection last + before query filter
    When a GraphQL query is executed
      """
        query {
          usersConnection(last: 2, before: "6") {
            pageInfo { hasNextPage hasPreviousPage startCursor endCursor }
            edges { cursor node { id name email active } }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "usersConnection": {
            "pageInfo": {
              "hasNextPage": false,
              "hasPreviousPage": true,
              "startCursor": "4",
              "endCursor": "5"
            },
            "edges": [
              { "cursor": "4", "node": { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true } },
              { "cursor": "5", "node": { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true } }
            ]
          }
        }
      }
      """