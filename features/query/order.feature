Feature: Order By Clause

  Be able to sort query results via GraphQL

  Background:
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
      }
      """
    And rows in the users table
      | id | name      | email        | active |
      | 1  | John Doe  | jd@acme.com  | false  |
      | 2  | Jane Doe  | jad@acme.com | false  |
      | 3  | Joe Doe   | jod@acme.com | true   |
      | 4  | Chris Doe | cd@acme.com  | true   |
      | 5  | Anne Doe  | ad@acme.com  | true   |
      | 6  | Peter Doe | pd@acme.com  | true   |

  Scenario: Order query results ascending
    When a GraphQL query is executed
      """
        query {
          users(orderBy: email_ASC) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true },
            { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false },
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """
  
  Scenario: Order query results descending

  Scenario: Order query results combined with where clause