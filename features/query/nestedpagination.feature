Feature: Query Pagination - nested relations

  Be able to specify SQL-like where clause elements via GraphQL

  Background:
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
        addresses: [Address!]
      }

      type Address {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name      | email        | active |
      | 1  | John Doe  | jd@acme.com  | false  |
    And rows in the addresses table
      | id | name         | street            | city        | state | zip   |
      | 1  | Office       | 123 Main St       | New York    | NY    | 11354 |
      | 2  | Home         | 123 Main St       | Los Angeles | CA    | 90012 |
      | 3  | Home         | 244 Colgate Ave   | Kensington  | CA    | 94708 |
      | 4  | Relatives    | 6736 Amethyst Ln  | Plano       | TX    | 75023 |
      | 5  | Kamado       | 1400 Shattuck Ave | Berkeley    | CA    | 94709 |
      | 6  | Chez Panisse | 1517 Shattuck Ave | Berkeley    | CA    | 94709 |
      | 7  | Cheeseboard  | 1512 Shattuck Ave | Berkeley    | CA    | 94709 |
    And rows in the _address_to_users table
      | id | user_id | address_id |
      | 1  | 1       | 1          |
      | 2  | 1       | 2          |
      | 3  | 1       | 3          |
      | 4  | 1       | 4          |
      | 5  | 1       | 5          |
      | 6  | 1       | 6          |
      | 7  | 1       | 7          |

  Scenario: Nested relation first query filter 
    When a GraphQL query is executed
      """
        query {
          users {
            id addresses(first: 2) {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1",
              "addresses": [
                { "id": "1", "name": "Office", "street": "123 Main St", "city": "New York", "state": "NY", "zip": "11354" },
                { "id": "2", "name": "Home", "street": "123 Main St", "city": "Los Angeles", "state": "CA", "zip": "90012" }
              ]
            }
          ]
        }
      }
      """

  Scenario: Nested relation after query filter
    When a GraphQL query is executed
      """
        query {
          users {
            id addresses(after: "5") {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            {
              "id": "1",
              "addresses": [
                { "id": "6", "name": "Chez Panisse", "street": "1517 Shattuck Ave", "city": "Berkeley", "state": "CA", "zip": "94709" },
                { "id": "7", "name": "Cheeseboard", "street": "1512 Shattuck Ave", "city": "Berkeley", "state": "CA", "zip": "94709" }
              ]
            }
          ]
        }
      }
      """

  Scenario: Nested relation first + after query filter
    When a GraphQL query is executed
      """
        query {
          users {
            id addresses(first: 2, after: "2") {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [{
            "id": "1",
            "addresses": [
              { "id": "3", "name": "Home", "street": "244 Colgate Ave", "city": "Kensington", "state": "CA", "zip": "94708" },
              { "id": "4", "name": "Relatives", "street": "6736 Amethyst Ln", "city": "Plano", "state": "TX", "zip": "75023" }
            ]
          }]
        }
      }
      """

  Scenario: Nested relation last query filter
    When a GraphQL query is executed
      """
        query {
          users {
            id addresses(last: 2) {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [{
            "id": "1",
            "addresses": [
              { "id": "6", "name": "Chez Panisse", "street": "1517 Shattuck Ave", "city": "Berkeley", "state": "CA", "zip": "94709" },
              { "id": "7", "name": "Cheeseboard", "street": "1512 Shattuck Ave", "city": "Berkeley", "state": "CA", "zip": "94709" }
            ]
          }]
        }
      }
      """

  Scenario: Nested relation before query filter
    When a GraphQL query is executed
      """
        query {
          users {
            id addresses(before: "3") {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [{
            "id": "1",
            "addresses": [
              { "id": "2", "name": "Home", "street": "123 Main St", "city": "Los Angeles", "state": "CA", "zip": "90012" },
              { "id": "1", "name": "Office", "street": "123 Main St", "city": "New York", "state": "NY", "zip": "11354" }
            ]
          }]
        }
      }
      """

  Scenario: Nested relation last + before query filter
    When a GraphQL query is executed
      """
        query {
          users {
            id addresses(last: 2, before: "6") {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [{
            "id": "1",
            "addresses": [
              { "id": "4", "name": "Relatives", "street": "6736 Amethyst Ln", "city": "Plano", "state": "TX", "zip": "75023" },
              { "id": "5", "name": "Kamado", "street": "1400 Shattuck Ave", "city": "Berkeley", "state": "CA", "zip": "94709" }
            ]
          }]
        }
      }
      """