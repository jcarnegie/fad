Feature: Mutate

  Dynamically generated mutations

  Scenario: Create new entity
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID!
        name: String!
        email: String!
      }
      """
    When a GraphQL query is executed
      """
      mutation {
        createUser(data: { name: "John Doe", email: "jd@acme.com" }) {
          id name email
        }
      }
      """
    Then the GraphQL query result createUser should have an id property
    And the GraphQL query should return json like
      """
      { "data": { "createUser": { "name": "John Doe", "email": "jd@acme.com" } } }
      """
    And a new row should be in the users table:
      |id|name    |email      |
      |* |John Doe|jd@acme.com|

  Scenario: Update entity
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID!
        name: String!
        email: String!
      }
      """
    And a row in the users table
      |id |name    |email      |
      |1  |John Doe|jd@acme.com|
    When a GraphQL query is executed
      """
      mutation {
        updateUser(data: { name: "Johnny Doe", email: "jd@whathappened.com" }, where: { id: "1" }) {
          id name email
        }
      }
      """
    Then the GraphQL query result updateUser should have an id property
    And the GraphQL query should return json like
      """
      { "data": { "updateUser": { "name": "Johnny Doe", "email": "jd@whathappened.com" } } }
      """
    And a new row should be in the users table:
      |id|name      |email              |
      |1 |Johnny Doe|jd@whathappened.com|

  Scenario: Delete entity
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID!
        name: String!
        email: String!
      }
      """
    And a row in the users table
      | id | name     | email       |
      | 1  | John Doe | jd@acme.com |
    When a GraphQL query is executed
      """
      mutation {
        deleteUser(where: { id: "1" }) {
          id name email
        }
      }
      """
    Then the GraphQL query result deleteUser should have an id property
    And the GraphQL query should return json like
      """
      { "data": { "deleteUser": { "name": "John Doe", "email": "jd@acme.com" } } }
      """
    And a row with id 1 in the users table is not found

  Scenario: Delete entity non-id selector in where clause
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
      }
      """
    And a row in the users table
      | id | name     | email       |
      | 1  | John Doe | jd@acme.com |
    When a GraphQL query is executed
      """
      mutation {
        deleteUser(where: { email: "jd@acme.com" }) {
          id name email
        }
      }
      """
    Then the GraphQL query result deleteUser should have an id property
    And the GraphQL query should return json like
      """
      { "data": { "deleteUser": { "name": "John Doe", "email": "jd@acme.com" } } }
      """
    And a row with id 1 in the users table is not found

  # Scenario: Update many entities
  #   Given a GraphQL server with typedefs
  #     """
  #     type User implements Node {
  #       id: ID!
  #       name: String!
  #       email: String!
  #       active: Boolean!
  #     }
  #     """
  #   And rows in the users table
  #     | id | name     | email        | active |
  #     | 1  | John Doe | jd@acme.com  | false  |
  #     | 2  | Jane Doe | jad@acme.com | false  |
  #     | 3  | Joe Doe  | jod@acme.com | true   |
  #   When a GraphQL query is executed
  #     """
  #     mutation {
  #       updateManyUsers(data: { active: true }, where: { active: false }) {
  #         count
  #       }
  #     }
  #     """
  #   Then the GraphQL query should return json like
  #     """
  #     { "data": { "updateManyUsers": { "count": 2 } } }
  #     """
  #   And the users table should have these rows:
  #     | id | name     | email        | active |
  #     | 1  | John Doe | jd@acme.com  | true   |
  #     | 2  | Jane Doe | jad@acme.com | true   |
  #     | 3  | Joe Doe  | jod@acme.com | true   |

  Scenario: Delete many entities
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID!
        name: String!
        email: String!
        active: Boolean!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | false  |
      | 2  | Jane Doe | jad@acme.com | false  |
      | 3  | Joe Doe  | jod@acme.com | true   |
    When a GraphQL query is executed
      """
      mutation {
        deleteManyUsers(where: { active: false }) {
          count
        }
      }
      """
    Then the GraphQL query should return json like
      """
      { "data": { "deleteManyUsers": { "count": 2 } } }
      """
    And the users table should have these rows:
      | id | name     | email        | active |
      | 3  | Joe Doe  | jod@acme.com | true   |

  Scenario: Upsert entity
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | false  |
    When a GraphQL query is executed
      """
      mutation {
        upsertUser(where: { email: "jd@acme.com" }, create: { name: "John Doe", email: "jd@acme.com", active: false }, update: { active: true }) {
          id name email active
        }
      }
      """
    Then the GraphQL query should return json like
      """
      { "data": { "upsertUser": { "name": "John Doe", "email": "jd@acme.com", "active": true } } }
      """
    And the users table should have these rows:
      | id | name     | email       | active |
      | 1  | John Doe | jd@acme.com | true   |