Feature: Enhanced Schema for Has One Relations

  Schema enhancements for has one relations.

  Scenario: Enhance typedef with has one create input type
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserCreateInput {
        name: String!
        email: String!
        address: AddressCreateOneInput
      }

      input AddressCreateOneInput {
        create: AddressCreateInput
        connect: AddressWhereUniqueInput
      }

      input AddressCreateInput {
        street: String!
        city: String!
        state: String!
        zip: String!
      }

      input AddressWhereUniqueInput {
        id: ID
      }
      """

  Scenario: Enhance typedef with bidirectional has one create input type
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
        user: User
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserCreateInput {
        name: String!
        email: String!
        address: AddressCreateOneWithoutUserInput
      }

      input AddressCreateOneWithoutUserInput {
        create: AddressCreateWithoutUserInput
        connect: AddressWhereUniqueInput
      }

      input AddressCreateWithoutUserInput {
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """

  # Scenario: Enhance typedef with has one create input type (bidirectional)
  #   Given a GraphQL schema definition
  #     """
  #     type User implements Node {
  #       id: ID! @unique
  #       name: String!
  #       email: 
  #     }
  #     """
  #   When the schema is enhanced
  #   Then the enhanced schema should contain enhanced definitions
  #     """
  #     input UserSubscriptionWhereInput {
  #       AND: [UserSubscriptionWhereInput!]
  #       OR: [UserSubscriptionWhereInput!]
  #       mutation_in: [MutationType!]
  #       updatedFields_contains: String
  #       updatedFields_contains_every: [String!]
  #       updatedFields_contains_some: [String!]
  #       node: UserWhereInput
  #     }
  #     """

  Scenario: Enhance typedef with has one update input type
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserUpdateInput {
        name: String
        email: String
        address: AddressUpdateOneInput
      }

      input AddressUpdateOneInput {
        create: AddressCreateInput
        connect: AddressWhereUniqueInput
        disconnect: Boolean
        delete: Boolean
        update: AddressUpdateDataInput
        upsert: AddressUpsertNestedInput
      }

      input AddressCreateInput {
        street: String!
        city: String!
        state: String!
        zip: String!
      }

      input AddressUpdateDataInput {
        street: String
        city: String
        state: String
        zip: String
      }

      input AddressUpsertNestedInput {
        update: AddressUpdateDataInput!
        create: AddressCreateInput!
      }

      input AddressWhereUniqueInput {
        id: ID
      }
      """

  Scenario: Enhance typedef with has one bidirectional update input type
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
        user: User
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserUpdateInput {
        name: String
        email: String
        address: AddressUpdateOneWithoutUserInput
      }

      input AddressUpdateOneWithoutUserInput {
        create: AddressCreateWithoutUserInput
        connect: AddressWhereUniqueInput
        disconnect: Boolean
        delete: Boolean
        update: AddressUpdateWithoutUserDataInput
        upsert: AddressUpsertWithoutUserNestedInput
      }

      input AddressCreateWithoutUserInput {
        street: String!
        city: String!
        state: String!
        zip: String!
      }

      input AddressUpdateWithoutUserDataInput {
        street: String
        city: String
        state: String
        zip: String
      }

      input AddressUpsertWithoutUserNestedInput {
        update: AddressUpdateWithoutUserDataInput!
        create: AddressCreateWithoutUserInput!
      }

      input AddressWhereUniqueInput {
        id: ID
      }
      """

  Scenario: Enhance typedef with has one non-null field
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        address: Address!
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserCreateInput {
        name: String!
        email: String!
        address: AddressCreateOneInput!
      }
      """