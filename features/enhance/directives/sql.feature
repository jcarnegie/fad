Feature: @sql field-level Directive

  @sql field-level directive

  Scenario: Modify field sql type 
    Given GraphQL typedefs
      """
      type User implements Node {
        id: ID!
        loginAttempts: Int @sql(type: "smallint")
      }
      """
    When migrations are run
    Then the users table should have columns
      | name           | type     | size | nullable | primary | unique |
      | id             | varchar  | 36   | false    | true    | true   |
      | login_attempts | smallint | 0    | true     | false   | false  |
