Feature: @crypted encryption Directive

  @crypted encryption field-level directive

  Scenario: Enhance schema with @crypted encryption directive
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID!
        password: String! @crypted
      }
      """
    When a GraphQL query is executed
      """
      mutation {
        createUser(data: { password: "password" }) {
          id password
        }
      }
      """
    Then the GraphQL query result createUser should have an id property
    And the GraphQL result field "data.createUser.password" should match "$argon2i$v=19$m=4096,t=3,p=1$"
    And a new row should be in the users table:
      |id|password            |
      |* |/\$argon2i\$v=19\$m=4096,t=3,p=1\$/|

  # Scenario: Enhance schema with @crypted encryption directive for nested create
  #   Given a GraphQL server with typedefs
  #     """
  #     type User implements Node {
  #       id: ID!
  #       password: String! @crypted
  #     }

  #     type Post implements Node {
  #       id: ID!
  #       content: String!
  #       author: User
  #     }
  #     """
  #   When a GraphQL query is executed
  #     """
  #     mutation {
  #       createPost(data: { content: "content", author: { create: { password: "password" } } }) {
  #         id content author { id password }
  #       }
  #     }
  #     """
  #   Then the GraphQL query result createPost should have an id property
  #   And the GraphQL result field "data.createPost.author.password" should match "$argon2i$v=19$m=4096,t=3,p=1$"
  #   And a new row should be in the users table:
  #     |id|password                           |
  #     |* |/\$argon2i\$v=19\$m=4096,t=3,p=1\$/|
  #   And a new row should be in the posts table:
  #     |id|content|
  #     |* |content|
