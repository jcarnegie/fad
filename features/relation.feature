Feature: @relation directive

  Use the @relation(name: "RelationName") directive to specify
  multiple relations between types and to avoid ambiguous relations.

  Scenario: Specify relation name
    Given GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
        address: Address @relation(name: "UserAddress")
      }

      type Address {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When migrations are run
    Then the database should have tables "users, addresses, _user_addresses, __migrations"