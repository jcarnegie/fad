Feature: Mutate with has one relation

  Make sure mutations support has one relations.

  Scenario: Create has one relation on create
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When a GraphQL query is executed
      """
        mutation {
          createUser(data: {
            name: "John Doe",
            email: "jd@acme.com",
            active: true,
            address: {
              create: {
                street: "123 Main St",
                city: "New York",
                state: "NY",
                zip: "11354"
              }
            }
          }) {
            id name email active address {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      {
        "data": {
          "createUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "address": {
              "street": "123 Main St",
              "city": "New York",
              "state": "NY",
              "zip": "11354"
            }
          }
        }
      }
      """
    And the users table should have rows that match:
      | id | name     | email       | active |
      | *  | John Doe | jd@acme.com | true   |
    And the addresses table should have rows that match:
      | id | street      | city     | state | zip   | 
      | *  | 123 Main St | New York | NY    | 11354 |
    And the _address_to_users table should have 1 rows

  Scenario: Connect has one relation on create
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the addresses table
      | id | street      | city        | state | zip   |
      | 1  | 123 Main St | New York    | NY    | 11354 |
    When a GraphQL query is executed
      """
        mutation {
          createUser(data: {
            name: "John Doe",
            email: "jd@acme.com",
            active: true,
            address: { connect: { id: "1" } }
          }) {
            id name email active address {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "createUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "address": {
              "street": "123 Main St",
              "city": "New York",
              "state": "NY",
              "zip": "11354"
            }
          }
        }
      }
      """
    And a new row should be in the users table:
      | id | name     | email       | active |
      | *  | John Doe | jd@acme.com | true   |
    And the _address_to_users table should have 1 rows

  Scenario: Create has one relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | street      | city        | state | zip   |
      | 1  | 123 Main St | New York    | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            address: {
              create: {
                street: "244 Colgate Ave",
                city: "Kensington",
                state: "CA",
                zip: "94708"
              }
            }
          }, where: {
            id: "1"
          }) {
            id name email active address {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "address": {
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }
          }
        }
      }
      """
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   | 
      | *  | 123 Main St     | New York   | NY    | 11354 |
      | *  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And the _address_to_users table should have 1 rows

  Scenario: Connect has one relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | street          | city       | state | zip   |
      | 1  | 123 Main St     | New York   | NY    | 11354 |
      | 2  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            address: {
              connect: { id: "2" }
            }
          }, where: {
            id: "1"
          }) {
            id name email active address {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "address": {
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }
          }
        }
      }
      """
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   | 
      | *  | 123 Main St     | New York   | NY    | 11354 |
      | *  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And the _address_to_users table should have rows that match:
      | id | address_id | user_id |
      | *  | 2          | 1       |

  Scenario: Disconnect has one relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | street          | city       | state | zip   |
      | 1  | 123 Main St     | New York   | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            address: {
              disconnect: true
            }
          }, where: {
            id: "1"
          }) {
            id name email active address {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "address": null
          }
        }
      }
      """
    And the users table should have rows that match:
      | id | name     | email       | active |
      | *  | John Doe | jd@acme.com | true   |
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   | 
      | *  | 123 Main St     | New York   | NY    | 11354 |
    And the _address_to_users table is empty

  Scenario: Delete has one relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | street          | city       | state | zip   |
      | 1  | 123 Main St     | New York   | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            address: {
              delete: true
            }
          }, where: {
            id: "1"
          }) {
            id name email active address {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "address": null
          }
        }
      }
      """
    And the users table should have rows that match:
      | id | name     | email       | active |
      | *  | John Doe | jd@acme.com | true   |
    And the addresses table is empty
    And the _address_to_users table is empty

  Scenario: Update has one relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | street          | city       | state | zip   |
      | 1  | 123 Main St     | New York   | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            address: {
              update: {
                street: "244 Colgate Ave",
                city: "Kensington",
                state: "CA",
                zip: "94708"
              }
            }
          }, where: {
            id: "1"
          }) {
            id name email active address {
              id street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "address": {
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }
          }
        }
      }
      """
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   | 
      | 1 | 244 Colgate Ave | Kensington | CA    | 94708 |
    And the _address_to_users table should have rows that match:
      | id | address_id | user_id |
      | 1  | 1          | 1       |

  Scenario: Upsert-create has one relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | false  |
    When a GraphQL query is executed
      """
      mutation {
        updateUser(
          data: {
            address: {
              upsert: {
                update: {
                  street: "123 Main St",
                  city: "New York",
                  state: "NY",
                  zip: "11354"
                },
                create: {
                  street: "244 Colgate Ave",
                  city: "Kensington",
                  state: "CA",
                  zip: "94708"
                }
              }
            }
          },
          where: { id: "1" }
        ) {
          id name email active address {
            id street city state zip
          }
        }
      }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": false,
            "address": {
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }
          }
        }
      }
      """
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   |
      | *  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And the _address_to_users table should have 1 rows

  Scenario: Upsert-update has one relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
        address: Address
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email       | active |
      | 1  | John Doe | jd@acme.com | true   |
    And rows in the addresses table
      | id | street      | city     | state | zip   |
      | 1  | 123 Main St | New York | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
    When a GraphQL query is executed
      """
      mutation {
        updateUser(
          data: {
            address: {
              upsert: {
                update: {
                  street: "244 Colgate Ave",
                  city: "Kensington",
                  state: "CA",
                  zip: "94708"
                },
                create: {
                  street: "123 Main St",
                  city: "New York",
                  state: "NY",
                  zip: "11354"
                }
              }
            }
          },
          where: { id: "1" }
        ) {
          id name email active address {
            id street city state zip
          }
        }
      }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "address": {
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }
          }
        }
      }
      """
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   |
      | 1  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And the _address_to_users table should have 1 rows
