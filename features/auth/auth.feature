Feature: Auth @auth directive

  Protect GraphQL endpoints and fields by requiring specific authorization roles

  Background:
    Given a GraphQL server with typedefs
      """
      type Post @auth(roles: ["admin"]) {
        id: ID! @unique
        title: String!
        contents: String!
        published: Boolean @default(value: false)
      }
      """

  Scenario: create method should return access denied when user doesn't have specified role
    When a GraphQL query is executed
      """
        mutation {
          createPost(data: { title: "Test", contents: "Empty" }) {
            id title contents published
          }
        }
      """
    Then the GraphQL query result should have error message "Access denied"

  Scenario: create method should allow access when user has specified role
    When the user is logged in with the admin role
    And a GraphQL query is executed
      """
        mutation {
          createPost(data: { title: "Test", contents: "Empty" }) {
            title contents published
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "createPost": {
            "title": "Test", "contents": "Empty", "published": false
          }
        }
      }
      """