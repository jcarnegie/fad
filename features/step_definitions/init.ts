/* eslint-disable */
import shell from 'shelljs';
import Docker from 'dockerode';
import bunyan from 'bunyan';
import uuid from 'uuid/v4';
import Promise, { all, promisify } from 'bluebird';
import { contains, curry, find, isNil, propSatisfies } from 'ramda';
import { After, AfterAll, Before, BeforeAll, setWorldConstructor } from 'cucumber';
import { Client } from 'pg';
import { dropAllTables } from '../../src/db/pg/ddl';
import { log } from '../../src/util/logging';

const CONTAINER_NAME = 'integration-testing-pg-db';
const PG_PASSWORD = 'pgpassword';
const PG_USER = 'postgres';
const PG_HOST = '127.0.0.1';
const PG_PORT = 6543;
const PG_URL = process.env.CI
  ? 'postgresql://postgres@127.0.0.1:5432/postgres'
  : 'postgresql://postgres:pgpassword@127.0.0.1:6543/postgres';

console.log(`Running cucumber tests against ${PG_URL}`);

const sleep = timeout =>
  new Promise(resolve => setTimeout(resolve, timeout));

const connect = async (
  connectionString = PG_URL,
  attempts = 30,
  interval = 1000,
) => {
  if (attempts === 0) throw new Error('Failed to connect');
  try {
    const client = new Client({
      connectionString
    });
    await client.connect();
    return client;
  } catch (e) {
    await sleep(interval);
    return connect(connectionString, attempts - 1, interval);
  }
};

const propContains = curry((property, value, object) =>
  propSatisfies(p => contains(value, p), property, object));

const docker = new Docker();
const listContainers = promisify(docker.listContainers.bind(docker));

const containerExists = containers =>
  !isNil(find(propContains('Names', `/${CONTAINER_NAME}`), containers));

const containerRunning = containers => {
  const container = find(propContains('Names', `/${CONTAINER_NAME}`), containers);
  return container && container.State === 'running';
};

// docker run -d -p 6543:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=pgpassword --name integration-testing-pg-db postgres
const runContainer = () =>
  `docker run -d -p ${PG_PORT}:5432 -e POSTGRES_USER=${PG_USER} -e POSTGRES_PASSWORD=${PG_PASSWORD} --name ${CONTAINER_NAME} postgres`;

const startContainer = () => `docker start ${CONTAINER_NAME}`;

// const stopContainer = () =>
//   `docker stop ${CONTAINER_NAME}`;

const silent = { silent: true };
let client = null;

BeforeAll({ timeout: 30000 }, async function() {
  try {
    // disable logging for tests
    log.level(bunyan.FATAL + 1);
    if (!process.env.CI) {
      const containers = await listContainers({ all: true });
      if (!containerExists(containers)) shell.exec(runContainer(), silent);
      if (!containerRunning(containers)) shell.exec(startContainer(), silent);
    }
    client = await connect();
  } catch (e) {
    console.log(e);
  }
});

AfterAll(async function() {
  await client.end();
});

setWorldConstructor(function() {
  this.client = client;
  this.dbCtx = { client, schemaName: 'trikata', idFn: uuid };
});

Before(async function (info) {
  await dropAllTables(this.dbCtx);
  await this.client.query(`create schema if not exists ${this.dbCtx.schemaName}`);
});

After(async function() {
  try {
    await dropAllTables(this.dbCtx);
  } catch (e) {
    console.log('After error:', e);
  }
})
