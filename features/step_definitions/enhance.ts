/* eslint-disable */
import { Given, When, Then } from 'cucumber';
import { expect } from 'chai';
import { parse, print } from 'graphql/language';
import { FutureInstance, map, promise } from 'fluture';
import {
  difference,
  equals,
  filter,
  find,
  join,
  length,
  map as rMap,
  path,
  pathEq,
  propEq,
  reduce,
  type,
  values
} from 'ramda';
import { init } from '../../src/db/pg';
import { enhance } from '../../src/schema/enhance';
import { ParseSchemaContext, createParseSchemaContext } from "../../src/schema/context";
import { propIn } from '../../src/util/object';

const containsTypedefs = (td1, td2) => {
  const ast1 = parse(td1, { noLocation: true });
  const ast2 = parse(td2, { noLocation: true });
  const gqlTypes = ['ObjectTypeDefinition', 'InputObjectTypeDefinition', 'EnumTypeDefinition', 'ScalarTypeDefinition'];
  const defs1 = filter(propIn('kind', gqlTypes), ast1.definitions);
  const defs2 = filter(propIn('kind', gqlTypes), ast2.definitions);

  rMap(def => {
    const matchingTypedef = find(pathEq(['name', 'value'], def.name.value), defs1);
    if (!matchingTypedef) {
      throw new Error(`'${print(def)}' not found in typedef`);
    }

    if (!equals(def.interfaces, matchingTypedef.interfaces)) {
      throw new Error(`Typedef ${def.name} doesn't implement correct interfaces`);
    }

    if (def.kind === 'EnumTypeDefinition') {
      const enumNames1 = rMap(path(['name', 'value']), matchingTypedef.values);
      const enumNames2 = rMap(path(['name', 'value']), def.values);
      const missingVals = difference(enumNames2, enumNames1);
      if (length(missingVals) > 0) {
        const formattedMissingVals = join(', ', missingVals);
        throw new Error(`${formattedMissingVals} not found in ${def.name.value} enum`);
      }
    } else {
      rMap(field => {
        const matchingField = find(pathEq(['name', 'value'], field.name.value), matchingTypedef.fields);
        if (!matchingField) {
          // throw AssertionError
          throw new Error(`Field '${print(field)}' not found in typedef`);
        }
        const msg = `Expected field ${def.name.value}.${field.name.value} to equal ${matchingTypedef.name.value}.${matchingField.name.value}`;
        expect(field).to.eql(matchingField, msg);
      }, def.fields || []);
    }
  }, defs2);
}

const noWS = s => s.replace(/\s+/g, '');

Given('a GraphQL schema definition', function (typedefs) {
  this.typedefs = typedefs;
});

When('the schema is enhanced', function () {
    const enhanceSchema = (parseContext: ParseSchemaContext): any => {
      const db = init(this.client);
      const context = { db };
      this.enhancedTypedefs = enhance(parseContext, this.typedefs);
    };
    return promise(
      map(enhanceSchema)(createParseSchemaContext())
    );
});

Then('the enhanced schema should contain general definitions', function (typedefs) {
  expect(noWS(this.enhancedTypedefs)).to.include(noWS(typedefs));
});

Then('the enhanced schema should contain enhanced definitions', function (typedefs) {
  // console.log(this.enhancedTypedefs);
  containsTypedefs(this.enhancedTypedefs, typedefs);
});

Then('the enhanced schema shouldn\'t contain enhanced definitions', function (typedefs) {
  // console.log(this.enhancedTypedefs);
  containsTypedefs(this.enhancedTypedefs, typedefs);
});

Then('the input type {word} shouldn\'t contain an {word} field', function (inputType, fieldName) {
  const ast = parse(this.enhancedTypedefs);
  const input = find(pathEq(['name', 'value'], inputType), ast.definitions);
  if (input) {
    const field = find(pathEq(['name', 'value'], fieldName), input.fields);
    if (field) {
      throw new Error(`${inputType} shouldn't contain the '${fieldName}' field`);
    }
  }
});